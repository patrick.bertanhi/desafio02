import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Desafio2LancamentosComponent } from './desafio2-lancamento/desafio2-lancamentos.component';


const routes: Routes = [
  { path: '',
  component: Desafio2LancamentosComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
