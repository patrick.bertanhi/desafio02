import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  constructor(private http: HttpClient) { }

  getLancamentos() {
   return this.http.get(`https://desafio-it-server.herokuapp.com/lancamentos/?_sort=mes_lancamento&amp;_order=desc`);
  }

}
