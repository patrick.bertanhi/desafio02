import { ConsultaService } from './../shared/service/consulta.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
@Component({
  selector: 'app-desafio2-lancamentos',
  templateUrl: './desafio2-lancamentos.component.html',
  styleUrls: ['./desafio2-lancamentos.component.scss']
})
export class Desafio2LancamentosComponent implements OnInit {

categorias = [
    'Transporte',
    'Compras Online',
    'Saúde e Beleza',
    'Serviços Automotivos',
    'Restaurantes',
    'Super Mercados'
  ];

gastos;
erro;

valoresAgrupados;


constructor(private consultaService: ConsultaService) { }

  ngOnInit() {
    this.getSpending();
  }

 getSpending() {
    this.consultaService.getLancamentos().subscribe(item  => {
      if (item) {
        this.gastos = this.atribuiMesCategoria(item);
        this.totalGastos(this.gastos);
      }

        }, (error: any) => {
            this.erro = error;
        });
}

  atribuiMesCategoria(dados) {
    // colocando o mês por escrito
    moment.locale('pt');
    dados = dados.map(item => {
        item.mes_lancamento = moment(item.mes_lancamento, 'MM').format('MMMM');
        return item;
    });

    // colocando categoria por escrito
    dados = dados.map(item => {
      item.categoria = this.categorias[item.categoria - 1];
      return item;
    });
    return dados;
  }

   totalGastos(contas: Array<any>) {

     this.valoresAgrupados = contas.reduce((acc, val, i, arr) => {

      // Verifica se a somatória já foi feita para este mês
      if (acc.find(accItem => accItem.mes_lancamento === val.mes_lancamento)) {
          return acc;
      }

      // Filtra somente os valores do mês e faz a somatória
      const sum = arr.filter(item => item.mes_lancamento === val.mes_lancamento).reduce((acc, val) => {
          return { mes_lancamento: acc.mes_lancamento, valor: acc.valor + val.valor }
      });

      // Retorna o objeto que contém a soma dentro de um novo array
      return [...acc, sum];
  }, []);
  }
}
