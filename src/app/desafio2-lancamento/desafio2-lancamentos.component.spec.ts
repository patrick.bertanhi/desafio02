import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Desafio2LancamentosComponent } from './desafio2-lancamentos.component';

describe('Desafio2LancamentosComponent', () => {
  let component: Desafio2LancamentosComponent;
  let fixture: ComponentFixture<Desafio2LancamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Desafio2LancamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Desafio2LancamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
