import { HttpClientModule } from '@angular/common/http';
import { ConsultaService } from './shared/service/consulta.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Desafio2LancamentosComponent } from './desafio2-lancamento/desafio2-lancamentos.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    Desafio2LancamentosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [ConsultaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
